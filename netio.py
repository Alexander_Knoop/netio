#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
	netio - Controll your IP-PDU
	Copyright (C) 2015 Alexander Knoop
	This program comes with ABSOLUTELY NO WARRANTY; for details see the LICENSE file.
	This is free software, and you are welcome to redistribute it under certain conditions; see LICENSE file for details.
	
	https://bitbucket.org/Alexander_Knoop/netio
"""

import urllib2, re

class pdu: 
	def __init__(self, ip, login, password):
		self.ip = ip
		self.__login = login
		self.__password = password
	
	def status(self):
		return request_status(self.ip, self.__login, self.__password)
	
	def toggle(self, channels):
		toggle_channels(self.ip, self.__login, self.__password, channels)
		return request_status(self.ip, self.__login, self.__password)
	
	def toggle_channel(self, channel):
		channels = ['u', 'u', 'u', 'u']
		channel = int(channel)-1
		channels[channel] = '2'
		toggle_channels(self.ip, self.__login, self.__password, channels)
		return request_status(self.ip, self.__login, self.__password)

def request_status(ip, login, password):
	# Get current status of the PDU
	# Create URL to CGI
	link = 'http://' + ip +'/cgi/control.cgi?login=p:' + login + ':' + password + '&p=l'
	# Load current state
	url = urllib2.urlopen(link)
	html = url.read()
	url.close()
	
	wert = re.findall(r'<html>(.*)</html>',html,re.S|re.I|re.M).pop()
	werte=wert.split()
	return werte

def send_comm(ip, login, password, werte):
	# Send command to PDU
	link = 'http://' + ip + '/cgi/control.cgi?login=p:' + login + ':' + password + '&p='
	befehl=''
	for wert in werte:
		befehl += wert

	url = urllib2.urlopen(link + befehl)
	url.close()

def toggle_channels(ip, login, password, werte):
	# process inputs before sending to PDU
	werte_alt = request_status(ip, login, password)
	werte_neu = ['u','u','u','u']
	master = 'u'
	i=0
	
	if '4' in werte:
		master = turn(werte_alt[werte.index('4')])
	
	# If value is 2, switch current state
	for wert in werte:
		if werte[i] == '2':
			werte_neu[i] = turn(werte_alt[i])
		elif werte[i] == '3':
			werte_neu[i] = master
		elif werte[i] == '4':
			werte_neu[i] = master
		else: 
			werte_neu[i] = werte[i]
		i= i+1
	send_comm(ip, login, password, werte_neu)

def turn(zahl):
	# Simply switches the given value
	ausgabe = (int(zahl)+1)%2
	return str(ausgabe)
