#!/usr/bin/env python
# -*- encoding: utf-8 -*-

from setuptools import setup

with open('README.md') as f:
	readme = f.read()

setup(
	name='netio',
	version='1.1',
	description='A small python module to control Koukaam PDUs.',
	url='https://bitbucket.org/Alexander_Knoop/netio',
	author='Alexander Knoop',
	author_email='computer.online@web.de',
	py_modules=['netio'],
	license='GPL-3.0',
	long_description=readme,
	)
