# netio #

This is a simple unofficial python module to control the IP-controllable PDU [NETIO-230B](http://www.koukaam.se/kkmd/showproduct.php?article_id=1502) from Koukaam.
It can control as many PDUs as you want.

## Installation ##

Install the module by downloading the .zip file under /dist/ and execute 

`pip install ./netio-X.x.zip`

Or you can simply copy the netio.py file to your project folder. 

Have a look at the examples to see how easy it is to control your PDU. 

## Stand-Alone-Example ##

You can use the netio_cmd.py to control the PDU without using python.
You have to modify the IP, Username and Password at the beginning of the file.

To control your PDU using cron, simply execute the example like this:

`python netio_cmd.py c 102u`

This turns channel 1 on, channel 2 off, switches the state of channel 3 and leaves channel 4 in it's current state.