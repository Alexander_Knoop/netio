#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Example File
Usage:
	Interactive:
		python example.py
	Switch single channel state (e.g. 4):
		python example.py 4
	Toggle all channels
		python example.py 1 u012
		u: Keep current state
		0: Turn channel off
		1: Turn channel on
		2: Switch state
		3: Slave (gets same state than master)
		4: Master (Switch state)
"""

import sys, netio

ip = '192.168.1.1'
username = 'username'
password = 'password'


license = [
	'netio - Controll your IP-PDU',
	'Copyright (C) 2015 Alexander Knoop',
	'This program comes with ABSOLUTELY NO WARRANTY; for details see the LICENSE file.',
	'This is free software, and you are welcome to redistribute it under certain conditions; see LICENSE file for details.'
	]
for line in license:
	print(line)


args = sys.argv

device1 = netio.pdu(ip,username,password)

if len(args)<2:
	print 'Channel: ',
	kanal = raw_input()
	device1.toggle_channel(kanal)
elif len(args)==2:
	kanal = args[1]
	device1.toggle_channel(kanal)
else:
	device1.toggle(args[2])
	
